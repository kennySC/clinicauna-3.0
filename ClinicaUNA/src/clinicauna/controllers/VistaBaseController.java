package clinicauna.controllers;

import clinicauna.ClinicaUNA;
import clinicauna.model.DetallecitaDto;
import clinicauna.model.UsuarioDto;
import clinicauna.services.DetalleCitaService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class VistaBaseController extends Controller implements Initializable {

    @FXML
    private VBox vbOpciones;
    @FXML
    private ImageView ivUserIcon;
    @FXML
    private JFXButton btnAgenda;
    @FXML
    private ImageView ivAgenda;
    @FXML
    private JFXButton btnRegPacientes;
    @FXML
    private ImageView ivPacientes;
    @FXML
    private JFXButton btnMantUsuarios;
    @FXML
    private ImageView ivMantUs;
    @FXML
    private JFXButton btnMantMedicos;
    @FXML
    private ImageView ivMantMed;
    @FXML
    private JFXButton btnExpediente;
    @FXML
    private ImageView ivExpediente;
    @FXML
    private Label lblNombre;
    @FXML
    private Label lblApellidos;
    @FXML
    private ImageView ivReportes;
    @FXML
    private BorderPane bpVistaBaseMod;

    UsuarioDto usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
//    public static String 
    public static String idioma = "E";

    public Stage pantalla;
    public boolean pantallaMin = false;
    public boolean pantallaMax = false;

    private List<DetallecitaDto> listCitas = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        bpVistaBase.setMinSize(840, 625);   
        idioma = (String) AppContext.getInstance().get("Idioma");
        lblNombre.setText(usuario.idpers.getNombrepers());
        lblApellidos.setText(usuario.idpers.getApellidospersona());
        if (usuario.getTipous().equals("A")) {

            ivUserIcon.setImage(new Image(ClinicaUNA.class.getResourceAsStream("resources/Admin.png")));
        }
        if (usuario.getTipous().equals("R")) {

            ivUserIcon.setImage(new Image(ClinicaUNA.class.getResourceAsStream("resources/recepcionist1.png")));
        }
        if (usuario.getTipous().equals("M")) {
            ivUserIcon.setImage(new Image(ClinicaUNA.class.getResourceAsStream("resources/stethoscope.png")));
        }        
           
//        comprobarCitas();

    }

    // OVERIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
    }

    @FXML
    private void PantallaAgenda(ActionEvent event) {
        FlowController.getInstance().goview("AgendaCitas");

    }

    @FXML
    private void PantallaRegistroPacientes(ActionEvent event) {
        FlowController.getInstance().goview("RegistroPacientes");

    }

    @FXML
    private void PantallaMantUsuarios(ActionEvent event) {
        if (usuario.getTipous().equals("A")) {
            FlowController.getInstance().goview("MantenimientoUsuarios");
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", stage,
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");
        }

    }

    @FXML
    private void PantallaMantMedicos(ActionEvent event) {
        if (usuario.getTipous().equals("A")) {
            FlowController.getInstance().goview("MantenimientoMedicos");

        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", stage,
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");
        }

    }

    @FXML
    private void PantallaExpedientes(ActionEvent event) {
        if (usuario.getTipous().equals("R")) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", stage,
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");
        } else {
            FlowController.getInstance().goview("Expedientes");
        }
    }

    @FXML
    private void PantallaReportes(ActionEvent event) {
        if (usuario.getTipous().equals("R")) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E") ? "Error de acceso" : idioma.equals("I") ? "Access Restriction" : "Erro de acesso", stage,
                    idioma.equals("E") ? "Tipo de usuario no puede acceder a esta parte del sistema." : idioma.equals("I") ? "This kind of user cannot access this part of the system." : "O tipo de usuário não pode acessar esta parte do sistema.");
        } else {
            FlowController.getInstance().goview("Reportes");
        }
    }

    private void comprobarCitas() {

        Respuesta res = new DetalleCitaService().getCitas();
        listCitas = (List<DetallecitaDto>) res.getResultado("Citas");
        ControllerCorreo cCorreo = new ControllerCorreo();
        Task correoCitas = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Timer temporizador = new Timer();
                temporizador.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                            
                        for (DetallecitaDto cita : listCitas) {
                            if (cita.getHoracita().isAfter(LocalDateTime.now()) && cita.getHoracita().isBefore(LocalDateTime.now().plusDays(2))
                                    && !cita.getEstadonotificada().equalsIgnoreCase("N")) {
                                String correo = cita.idpaciente.idpers.getCorreopers();
                                cCorreo.getCorreo().formatoPaciente(cita, "Recordatorio");
                                Respuesta resCorreo = cCorreo.enviarEmailHTML(correo);
                                if (resCorreo.getEstado()) {
                                    System.out.println("Cita Notificada");
                                }
                            } else if(cita.getHoracita().isBefore(LocalDateTime.now()) && cita.getEstadocita().equalsIgnoreCase("Pr")){
                                cita.setEstadocita("Au");
                                Respuesta res = new DetalleCitaService().guardarDetCita(cita);
                            }

                        }

                    }
                }, 200, (long) Duration.minutes(40).toMillis());
                return null;
            }

        };
        Thread hiloCitas = new Thread(correoCitas);
        hiloCitas.setDaemon(Boolean.TRUE);
        hiloCitas.start();

    }

}
