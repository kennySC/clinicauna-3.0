package clinicauna.controllers;

import clinicauna.model.UsuarioDto;
import clinicauna.services.UsuariosService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class LogInController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXPasswordField txtClave;
    @FXML
    private JFXButton btnIngresar;
    @FXML
    private JFXButton btnSalir;
    @FXML
    private JFXButton btnOlvidoContra;
    @FXML
    private VBox vbLogIn;
    @FXML
    private ImageView ivTopLogin;
    @FXML
    private ImageView ivUserLogin;
    @FXML
    private ImageView ivPassLogin;
    @FXML
    private StackPane spParent;

    String idioma = (String) AppContext.getInstance().get("Idioma");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        formatoTextos();
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        txtClave.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    IngresarSistema(new ActionEvent());
                }
            }
        });
    }

    // METODO PARA EL BOTON DE INGRESAR/LOGIN //
    @FXML
    private void IngresarSistema(ActionEvent event) {
        try {
            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                Mensaje.showModal(Alert.AlertType.ERROR, "Validación de usuario", getStage(), "Es necesario digitar un usuario para ingresar al sistema.");
            } else if (txtClave.getText() == null || txtClave.getText().isEmpty()) {
                Mensaje.showModal(Alert.AlertType.ERROR, "Validación de usuario", getStage(), "Es necesario digitar una contraseña para ingresar al sistema.");
            } else {
                UsuariosService usService = new UsuariosService();
                Respuesta respuesta = usService.validarUsuario(txtUsuario.getText(), txtClave.getText());
                if (respuesta.getEstado()) {
                    UsuarioDto us = (UsuarioDto) respuesta.getResultado("Usuario");
                    AppContext.getInstance().set("Usuario", us);
                    if (us.getEstadous().equals("A")) {
                        if (isContrasenaTemporal(us.getPasswordus())) {
                            FlowController.getInstance().goviewInWindow("CambiarContrasena");
                        } else {
                            //System.out.println(us.idpers.getNombrepers());
                            prepararApp(us);
                        }
                        getStage().close();
                    } else {
                        Mensaje.showModal(Alert.AlertType.ERROR, idioma.equals("E")?"Ingreso Invalido":idioma.equals("I")?"Invalid LogIn":"Renda inválida", getStage(), idioma.equals("E")?"Su usuario no esta activado.\nIngrese a su correo para activarlo.":idioma.equals("I")?"Your user has not yet been activated.\nCheck your email to activate it.":"Seu usuário não está ativado.\nDigite seu e-mail para ativá-lo.");
                    }
                } // USUARIO NO FUE ENCONTRADO O NO EXISTE //
                else {
                    Mensaje.showModal(Alert.AlertType.ERROR,"Error", getStage(), idioma.equals("E")?"Usuario no encontrado.":idioma.equals("I")?"User not found.":"Usuário não encontrado.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }

    }

    // METODO PARA EL BOTON DE QUE SE LE OLVIDO LA CONTRASENNA //
    @FXML
    private void RecuperarContrasenna(ActionEvent event) {
        if (!txtUsuario.getText().isEmpty()) {

            Respuesta res = new UsuariosService().recuperarContrasenaUsuario(txtUsuario.getText());
            if (res.getEstado()) {
                UsuarioDto usuario = (UsuarioDto) res.getResultado("Usuario");
                new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.equals("E")?"Exito":idioma.equals("I")?"Success":"Êxito", this.getStage(), idioma.equals("E")?"Se le ha enviado un correo con una contraseña temporal.\nIngrese al sistema con ella y podra proceder a ingresar una nueva contraseña."
                        :idioma.equals("I")?"A temporary password has been sent to your email.\nUse it to log in and then you may change your password.":"Você recebeu um e-mail com uma senha temporária.\nFaça login com ele e poderá prosseguir para inserir uma nova senha.");
                String correo = usuario.idpers.getCorreopers();
                ControllerCorreo cCorreo = new ControllerCorreo();
                Task tarea = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        cCorreo.getCorreo().formatoCambioContrasena(usuario);
                        Respuesta resCorreo = cCorreo.enviarEmailHTML(correo);
                        return null;
                    }
                };
                Thread hilo = new Thread(tarea);
                hilo.start();
            } else {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Error", this.getStage(), res.getMensaje());
            }

        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Error", getStage(), idioma.equals("E")?"Porfavor ingrese su nombre de usuario.":idioma.equals("I")?"Please enter your username":"Por favor, digite seu nome de usuário.");
        }
    }

    public Boolean isContrasenaTemporal(String password) {
        if (password.startsWith("cT") && password.endsWith("Tc")) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public void prepararApp(UsuarioDto us) {
        AppContext.getInstance().set("Idioma", us.getIdiomaus());
        FlowController.CambiarIdioma(us.getIdiomaus());
        FlowController.getInstance().goMain();
        FlowController.getInstance().goview("PantallaPrincipal");
    }

    public void formatoTextos() {
        txtUsuario.setTextFormatter(Formato.getInstance().maxLengthFormat(30));
        txtClave.setTextFormatter(Formato.getInstance().maxLengthFormat(30));
    }

    @FXML
    private void SalirLogIn(ActionEvent event) {
        System.exit(0);
    }

}
