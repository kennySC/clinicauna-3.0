package clinicauna.controllers;

import clinicauna.model.MedicoDto;
import clinicauna.services.ReportesService;
import clinicauna.util.AppContext;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class GenerarReporteAgendaController extends Controller implements Initializable {

    @FXML
    private ImageView imgReportes;
    @FXML
    private JFXButton btnGenerar;
    @FXML
    private JFXDatePicker dpFechaIni;
    @FXML
    private JFXDatePicker dpFechaFin;

    String idioma = (String) AppContext.getInstance().get("Idioma");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    // OVERRIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {

    }

    @FXML
    private void generarReporte(ActionEvent event) {

        if (dpFechaIni.getValue() != null && dpFechaFin.getValue() != null) {
            String fecha1 = dpFechaIni.getValue().toString();
            String fecha2 = dpFechaFin.getValue().toString();
            System.out.println(fecha1 + " | " + fecha2);
            MedicoDto med = (MedicoDto) AppContext.getInstance().get("medicoSelect");
            Respuesta res = new ReportesService().reporteAgenda(med.getIdmed(), fecha1, fecha2);
            if (res.getEstado()) {
                JasperPrint report = (JasperPrint) res.getResultado("reporte");            
                JasperViewer rViwer = new JasperViewer(report, false);                    
                rViwer.setVisible(true);
            } else {

            }
        } else {
            Mensaje.showModal(Alert.AlertType.ERROR, "Error", getStage(), idioma.equals("E") ? "Ambas fechas deben estar llenas" : idioma.equals("I") ? "Both dates must be filled" : "portugezinho");
        }

    }

}
