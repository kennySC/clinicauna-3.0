package clinicauna.controllers;

import clinicauna.model.EncabezadoexpedienteDto;
import clinicauna.model.ExamenespacienteDto;
import clinicauna.model.RegistropacientesDto;
import clinicauna.services.ExamenPacienteService;
import clinicauna.services.PacienteService;
import clinicauna.util.AppContext;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * FXML Controller class
 *
 * @author Matthew Miranda
 */
public class AdjuntarExamenPacienteController extends Controller implements Initializable {

    String idioma = (String) AppContext.getInstance().get("Idioma");

    @FXML
    private JFXTextField txtNombreExamPaciente;
    @FXML
    private JFXDatePicker dpExamPaciente;
    @FXML
    private JFXButton btnAdjuntarExamen;
    @FXML
    private JFXButton btnGuardarExamen;
    @FXML
    private JFXTextArea txtAnotaciones;
    @FXML
    private TableColumn<ExamenespacienteDto, LocalDate> TC_Fecha;
    @FXML
    private ImageView ivExamen;    
    @FXML
    private TableView<ExamenespacienteDto> tvExamenes;
    
    private List<ExamenespacienteDto> examenes = new ArrayList();
    private EncabezadoexpedienteDto expediente;
    private RegistropacientesDto paciente;
    private ExamenespacienteDto examen;
    private File file;
    @FXML
    private Label lblArchivo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        txtNombreExamPaciente.setEditable(false);
        dpExamPaciente.setEditable(false);
        TC_Fecha.setCellValueFactory(value->new SimpleObjectProperty<>(value.getValue().getFechaexamp()));
        tvExamenes.setOnMouseClicked(e -> {
            if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
                if (tvExamenes.getSelectionModel().getSelectedItem() != null) {
                    nuevoExamen();
                    try {
                        examen = tvExamenes.getSelectionModel().getSelectedItem();
                        bindExamen();
                        lblArchivo.setText(examen.getNombreexamp());
                        ivExamen.setImage(bufferedImage(examen.getArchivoexamp()));
                    } catch (IOException ex) {
                        Logger.getLogger(AdjuntarExamenPacienteController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    // OVERIDE GENERADO POR EL EXTENDS CONTROLLER //
    @Override
    public void initialize() {
        file = null;
        examen = new ExamenespacienteDto();
        expediente = (EncabezadoexpedienteDto) AppContext.getInstance().get("EncabExpE");
        paciente = expediente.getIdpaciente();
        txtNombreExamPaciente.setText(paciente.getIdpers().getNombrepers()+" "+paciente.getIdpers().getApellidospersona());
        AppContext.getInstance().delete("EncabExpE");
        nuevoExamen();
       cargarTablaExamenes();

    }

    // METODO PARA SUBIR/ADJUNTAR EL EXAMEN DEL PACIENTE //
    @FXML
    private void adjuntarExamenPaciente(ActionEvent event) throws IOException {
        FileChooser fchooser = new FileChooser();
        file = fchooser.showOpenDialog(null);
        if (file != null) {
            examen.setArchivoexamp(encoder(file.getPath()));
            examen.setNombreexamp(file.getName()); 
            lblArchivo.setText(file.getName());
        }

    }

    // GUARDAR EL EXAMEN ADJUNTADO //
    @FXML
    private void guardarExamenAdjuntado(ActionEvent event) {
        if(!validar()){
            new Mensaje().showModal(Alert.AlertType.ERROR, idioma.equals("E") ? "Error Informacion " : idioma.equals("I") ? "Info Error" : "Erro Informações ", this.getStage(),
                    idioma.equals("E") ? "Porfavor revise la informacion, hay campos vacios o campos con atributos no validos." : idioma.equals("I") ? "Please check the information, there could be empty spaces or invalid text fields." : "Por favor, verifique as informações, existem campos vazios ou com atributos inválidos.");
        }else{
        bindExamen();
        examen.setIdencabexp(expediente);
        examen.toString();
        Respuesta res = new ExamenPacienteService().guardarExamPaciente(examen);
        if(res.getEstado()){
            nuevoExamen();
            cargarTablaExamenes();
        }else{
           new Mensaje().show(Alert.AlertType.ERROR, "Error", idioma.equals("E") ? "Error al guardar el examen" : idioma.equals("I") ? "Error saving the exam."
                               : "Erro ao salvar o exame.");
        }
        }
    }

    public void bindExamen() {
        txtAnotaciones.textProperty().bindBidirectional(examen.anotacionesexamp);
        dpExamPaciente.valueProperty().bindBidirectional(examen.fechaexamp);
    }

    public void unbindExamen() {
        txtAnotaciones.textProperty().unbindBidirectional(examen.anotacionesexamp);
        dpExamPaciente.valueProperty().unbindBidirectional(examen.fechaexamp);
    }
    public void cargarTablaExamenes(){
        tvExamenes.getItems().clear();
        Respuesta res = new ExamenPacienteService().getExamenes(expediente.getIdencabexp());
        if(res.getEstado()){
        examenes =(List<ExamenespacienteDto>)res.getResultado("Examenes");
        tvExamenes.getItems().addAll(examenes);
        }
        
    }

    public void nuevoExamen() {
        lblArchivo.setText("");
        unbindExamen();
        file = null;
        examen = new ExamenespacienteDto();
        txtAnotaciones.clear();
        dpExamPaciente.setValue(null);
        bindExamen();
    }
public String encoder(String imagePath) {
        String base64Image = "";
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
        } catch (FileNotFoundException e) {
           new Mensaje().show(Alert.AlertType.ERROR, "Error", idioma.equals("E") ? "Hubo un error al cargar la imagen." : idioma.equals("I") ? "An error ocurred while loading the image."
                        : "Houve um erro ao carregar o imagem.");
        } catch (IOException ioe) {
                       new Mensaje().show(Alert.AlertType.ERROR, "Error", idioma.equals("E") ? "Hubo un error al leer la imagen." : idioma.equals("I") ? "An error ocurred while reading the image."
                               : "Houve um erro ao ler o imagem.");

        }
        return base64Image;
    }

 Image bufferedImage(String base64Image) throws IOException {
        byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
        BufferedImage image = null;
        InputStream in = new ByteArrayInputStream(imageByteArray);
        image = ImageIO.read(in);
        return convertToFxImage(image);
    }
 
    private Image convertToFxImage(BufferedImage image) {
        WritableImage wr = null;
        if (image != null) {
            wr = new WritableImage(image.getWidth(), image.getHeight());
            PixelWriter pw = wr.getPixelWriter();
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    pw.setArgb(x, y, image.getRGB(x, y));
                }
            }
        }

        return wr;
    }
    public boolean validar(){
        if(txtAnotaciones.getText()==null||txtAnotaciones.getText().isEmpty()){
            return false;
        }
        if(txtNombreExamPaciente.getText()==null||txtNombreExamPaciente.getText().isEmpty()){
            return false;
        }
        if(file==null){
            return false;
        }
        if(dpExamPaciente.getValue()==null){
            return false;
        }
        return true;
    }
}
