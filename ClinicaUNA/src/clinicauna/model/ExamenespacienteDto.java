// DTO DEL PROYECTO NORMAL //  
package clinicauna.model;

import clinicauna.util.LocalDateAdapter;
import java.io.Serializable;
import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Matthew Miranda
 */
@XmlRootElement(name = "ExamenespacienteDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class ExamenespacienteDto {

    @XmlTransient
    public SimpleStringProperty idexamp;
    @XmlTransient
    public SimpleStringProperty nombreexamp;
    @XmlTransient
    public ObjectProperty<LocalDate> fechaexamp;
    @XmlTransient
    public SimpleStringProperty anotacionesexamp;
    @XmlTransient
    public SimpleStringProperty archivoexamp;

    @XmlTransient
    public EncabezadoexpedienteDto idencabexp;   // FK DEL DETALLE DE EXPEDIENTE //

    public ExamenespacienteDto() {
        this.idexamp = new SimpleStringProperty();
        this.nombreexamp = new SimpleStringProperty();
        this.fechaexamp = new SimpleObjectProperty();
        this.anotacionesexamp = new SimpleStringProperty();
        this.archivoexamp = new SimpleStringProperty();
        this.idencabexp = new EncabezadoexpedienteDto(); // FK DEL DETALLE DE EXPEDIENTE //
    }

    public Long getIdexamp() {
        if (idexamp.get() != null && !idexamp.get().isEmpty()) {
            return Long.valueOf(idexamp.get());
        } else {
            return null;
        }
    }

    public void setIdexamp(Long idexamp) {
        this.idexamp.set(idexamp.toString());
    }

    // FK DEL DETALLE DE EXPEDIENTE //
    public EncabezadoexpedienteDto getIdencabexp() {
        return idencabexp;
    }

    public void setIdencabexp(EncabezadoexpedienteDto iddetexp) {
        this.idencabexp = iddetexp;
    }

    public String getNombreexamp() {
        return nombreexamp.get();
    }

    public void setNombreexamp(String nombreexamp) {
        this.nombreexamp.set(nombreexamp);
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getFechaexamp() {
        return fechaexamp.get();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setFechaexamp(LocalDate fechaexamp) {
        this.fechaexamp.set(fechaexamp);
    }

    public String getAnotacionesexamp() {
        return anotacionesexamp.get();
    }

    public void setAnotacionesexamp(String anotacionesexamp) {
        this.anotacionesexamp.set(anotacionesexamp);
    }

    @Override
    public String toString() {
        return "ExamenespacienteDto{" + "idexam = " + idexamp + ", nombreexamp = " + nombreexamp
                + ", fechaexamp = " + fechaexamp + ", anotacionesexamp = " + anotacionesexamp
                + ", FK iddetexp = " + idencabexp + '}';
    }

    public String getArchivoexamp() {
        return archivoexamp.get();
    }

    public void setArchivoexamp(String archivoexamp) {
        this.archivoexamp.set(archivoexamp);
    }

}
