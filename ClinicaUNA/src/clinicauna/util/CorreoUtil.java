/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.util;


public final class CorreoUtil {
    
    public static Boolean verificarCorreo(String correo) {
        if (!correo.contains("@")) {
            return false;
        } else {
            Integer i = correo.indexOf("@");
            Integer q = correo.indexOf(".", i);
            
            if(i <= 0){
                return false;
            }
            
            if ( q < 0) {
                return false;
            }          
            if((q - i) <= 1){
                return false;
            }
            
            if(correo.length()-1 <= q ){
                return false;
            }
            

        }
        return true;
    }
    
}
