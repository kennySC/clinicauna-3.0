
package clinicauna.util;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.stage.Window;
import clinicauna.controllers.VistaBaseController;

/**
 *
 * @author carranza
 */
public /*abstract*/ class Mensaje{

    public static void show(AlertType tipo,String titulo,String mensaje) {
        
        Alert alert=new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.setContentText(mensaje);
        if(VistaBaseController.idioma.equals("I")){
          alert.getButtonTypes().set(0, new ButtonType("OK", ButtonBar.ButtonData.OK_DONE));
         }else if(VistaBaseController.idioma.equals("E")){
           alert.getButtonTypes().set(0, new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE));  
        }else{
             alert.getButtonTypes().set(0, new ButtonType("Aceitar", ButtonBar.ButtonData.OK_DONE)); 
        }
        alert.show();
    }
public static void showImage(AlertType tipo,String titulo,String mensaje) {
		ImageView image= new ImageView("juegoPreguntados/resources/ciencia.png");
		image.setFitHeight(100);
		image.setFitWidth(100);
		image.setPreserveRatio(true);
        Alert alert=new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.setContentText(mensaje);
		alert.setGraphic(image);
        alert.show();
    }
    public static void showModal(AlertType tipo,String titulo,Window padre,String mensaje) {
        Alert alert=new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.initOwner(padre);
        alert.setContentText(mensaje);
        if(VistaBaseController.idioma.equals("I")){
          alert.getButtonTypes().set(0, new ButtonType("OK", ButtonBar.ButtonData.OK_DONE));
         }else if(VistaBaseController.idioma.equals("E")){
           alert.getButtonTypes().set(0, new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE));  
        }else{
             alert.getButtonTypes().set(0, new ButtonType("Aceitar", ButtonBar.ButtonData.OK_DONE)); 
        }
        alert.showAndWait();
    }
    
    public static Boolean showConfirmation(String titulo,Window padre,String mensaje) {
        Alert alert=new Alert(AlertType.CONFIRMATION, mensaje, new ButtonType("OK", ButtonBar.ButtonData.YES),new ButtonType("Cancel", ButtonBar.ButtonData.NO));
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.initOwner(padre);
        alert.setContentText(mensaje);
        if(VistaBaseController.idioma.equals("I")){
          alert.getButtonTypes().set(0, new ButtonType("OK", ButtonBar.ButtonData.YES));
          alert.getButtonTypes().set(1, new ButtonType("Cancel", ButtonBar.ButtonData.NO));
         }else if(VistaBaseController.idioma.equals("E")){
           alert.getButtonTypes().set(0, new ButtonType("Aceptar", ButtonBar.ButtonData.YES)); 
           alert.getButtonTypes().set(1, new ButtonType("Cancelar", ButtonBar.ButtonData.NO)); 
        }else{
             alert.getButtonTypes().set(0, new ButtonType("Aceitar", ButtonBar.ButtonData.YES)); 
             alert.getButtonTypes().set(1, new ButtonType("Cancelar", ButtonBar.ButtonData.NO));
        }
        
        Optional<ButtonType>result=alert.showAndWait();
        return result.get().getButtonData()==ButtonBar.ButtonData.YES;
    }
}
