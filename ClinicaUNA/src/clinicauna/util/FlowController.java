
package clinicauna.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.animation.FadeTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import clinicauna.controllers.Controller;
import clinicauna.ClinicaUNA;

public class FlowController {

    private static FlowController INSTANCE = null;
    private static Stage mainStage;
    private static ResourceBundle idioma;
    private static HashMap<String, FXMLLoader> loaders = new HashMap<>();
    private Stage stage;

    private FlowController() {
        
    }

    public static Stage getMainStage() {
        return mainStage;
    }

    public static void setMainStage(Stage mainStage) {
        FlowController.mainStage = mainStage;
    }

    private static void createInstance() {
        if (INSTANCE == null) {
            synchronized (FlowController.class) {
                if (INSTANCE == null) {
                    INSTANCE = new FlowController();
                }
            }
        }
    }

    public static FlowController getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public void InitializeFlow(Stage stage, ResourceBundle idioma) {
        getInstance();
        this.mainStage = stage;
        this.idioma = idioma;
    }

    private FXMLLoader getLoader(String name) {
        FXMLLoader loader = loaders.get(name);
        if (loader == null) {
            synchronized (FlowController.class) {
                if (loader == null) {
                    try {
                        loader = new FXMLLoader(ClinicaUNA.class.getResource("views/" + name + ".fxml"), this.idioma);
                        loader.load();
                        loaders.put(name, loader);
                    } catch (Exception ex) {
                        loader = null;
                        java.util.logging.Logger.getLogger(FlowController.class.getName()).log(Level.SEVERE, "Creando loader [" + name + "].", ex);
                    }
                }
            }
        }
        return loader;
    }
    
    public void goMain() {
        try {
            this.mainStage.setResizable(true);
            this.mainStage.setScene(new Scene(FXMLLoader.load(ClinicaUNA.class.getResource("views/VistaBase.fxml"), this.idioma)));
            this.mainStage.getIcons().add(new Image(ClinicaUNA.class.getResourceAsStream("resources/IconoClinicaUna.png")));
            this.mainStage.show();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FlowController.class.getName()).log(Level.SEVERE, "Error inicializando la vista base.", ex);
        }
    }
    
    public void goview(String viewName) {
        goview(viewName, "Center", null);
    }

    public void goview(String viewName, String accion) {
        goview(viewName, "Center", accion);
    }

    public void goview(String viewName, String location, String accion) {
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        controller.setAccion(accion);
        controller.initialize();
        stage = controller.getStage();
        if (stage == null) {
            stage = this.mainStage;
            controller.setStage(stage);
        }
        switch (location) {
            case "Center":
                FadeTransition trans1 = new FadeTransition(Duration.millis(350), ((StackPane) ((BorderPane) stage.getScene().getRoot()).getCenter()));
                trans1.setByValue(-1);
                trans1.setOnFinished(f -> {
                ((StackPane) ((BorderPane) stage.getScene().getRoot()).getCenter()).getChildren().clear();
                ((StackPane) ((BorderPane) stage.getScene().getRoot()).getCenter()).getChildren().add(loader.getRoot());
                FadeTransition trans2 = new FadeTransition(Duration.millis(350), ((StackPane) ((BorderPane) stage.getScene().getRoot()).getCenter()));
                    trans2.setByValue(1);
                    trans2.play();
                });
                trans1.play();
                break;
            case "Top":
                break;
            case "Bottom":
                break;
            case "Right":
                break;
            case "Left":
                break;
            default:
                break;
        }
    }

    public void goviewInStage(String viewName, Stage stage) {
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        controller.setStage(stage);
        stage.getScene().setRoot(loader.getRoot());
    }

    public void goviewInWindow(String viewName) {
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        controller.initialize();
        Stage stage = new Stage();
//        stage.getIcons().add(new Image("clinicauna/resources/user.png"));
        stage.setTitle("ClinicaUNA");
        stage.setOnHidden((WindowEvent event) -> {
            controller.getStage().getScene().setRoot(new Pane());
            controller.setStage(null);
        });
        controller.setStage(stage);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();

    }

    public void goviewInWindowModal(String viewName, Stage parentStage, Boolean resizable) {
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        controller.initialize();
        Stage stage = new Stage();
        stage.getIcons().add(new Image(ClinicaUNA.class.getResourceAsStream("resources/IconoClinicaUna.png")));
//        stage.setTitle("FILTRAR");
        stage.setResizable(resizable);
        stage.setOnHidden((WindowEvent event) -> {
            controller.getStage().getScene().setRoot(new Pane());
            controller.setStage(null);
        });
        controller.setStage(stage);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(parentStage);
        stage.centerOnScreen();
        stage.showAndWait();

    }
    public void goviewInWindowModalUndecorated(String viewName, Stage parentStage, Boolean resizable) {
        FXMLLoader loader = getLoader(viewName);
        Controller controller = loader.getController();
        controller.initialize();
        Stage stage = new Stage();
//        stage.getIcons().add(new Image("preguntados/Resources/IconoTitulo.png"));
//        stage.setTitle("PREGUNTADOS");
        stage.setResizable(resizable);
        stage.setOnHidden((WindowEvent event) -> {
            controller.getStage().getScene().setRoot(new Pane());
            controller.setStage(null);
        });
        controller.setStage(stage);
        Parent root = loader.getRoot();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(parentStage);
        stage.centerOnScreen();
        stage.show();

    }

    public Controller getController(String viewName) {
        return getLoader(viewName).getController();
    }

    public static void setIdioma(ResourceBundle idioma) {
        FlowController.idioma = idioma;
    }
    
    public static void CambiarIdioma(String i) {
    ResourceBundle idiomas ;
    if (i.equals("E")) {
        idiomas = ResourceBundle.getBundle("clinicauna.resources.i18n/idioma_ES");
    } else if(i.equals("I")){
        idiomas = ResourceBundle.getBundle("clinicauna.resources.i18n/idioma_EN");
    }else 
    {
       idiomas = ResourceBundle.getBundle("clinicauna.resources.i18n/idioma_PORT");  
    }
    setIdioma(idiomas);
    }
    
    public void initialize() {
        this.loaders.clear();
    }

    public void salir() {
        this.mainStage.close();
    }
    public void cerrar(Stage stage){
        stage.close();
    }
    
    public void desvincular(String viewName){
        loaders.remove(viewName);
    }

}
