
package clinicauna;

import clinicauna.util.FlowController;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Matthew Miranda
 */
public class ClinicaUNA extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        
        FlowController.getInstance().InitializeFlow(stage,null);
        stage.setTitle("CLINICAUNA");
        FlowController.CambiarIdioma("E");
        FlowController.getInstance().goviewInWindowModal("LogIn", stage, Boolean.TRUE);
//        FlowController.getInstance().goMain();
//        FlowController.getInstance().goview("LogIn");
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
