
package clinicauna.services;

import clinicauna.model.PersonaDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matthew Miranda
 */

public class PersonaService {
    
    public Respuesta guardarPersona(PersonaDto persDto){
        try{
            Request req = new Request("PersonaController/guardarPersona");
            req.post(persDto);
            if(req.isError()){
                return new Respuesta(false, req.getError(), "Error en la persona");
            }
            PersonaDto persona = (PersonaDto) req.readEntity(PersonaDto.class);
            return new Respuesta(true, "Exito", "", "Persona", persona);
        }
        catch(Exception e){
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error guardando la persona ", e);
            return new Respuesta(false, "Error guardando la persona.", "guardarPersona " + e.getMessage());
        }
    }
    
    public Respuesta eliminarPersona(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("PersonaController/eliminarPersona","/{id}",parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(PersonaService.class.getName()).log(Level.SEVERE, "Error eliminando a la persona.", ex);
            return new Respuesta(false, "Error eliminando la persona.", "eliminarPersona " + ex.getMessage());
        }
    }
    
}
