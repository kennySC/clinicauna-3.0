/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.services;

import clinicauna.model.ExamenespacienteDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author DJork
 */
public class ExamenPacienteService {
    public Respuesta guardarExamPaciente(ExamenespacienteDto exampacDto) {

        try {
            Request request = new Request("ExamenesPacienteController/guardarExamPaciente");
            request.post(exampacDto);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "Error al guardar el examen");
            }
            ExamenespacienteDto examenDto = (ExamenespacienteDto) request.readEntity(ExamenespacienteDto.class);

            return new Respuesta(true, "Examen guardado con exito", "", "Expediente", examenDto);

        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error guardando el examen ", ex);
            return new Respuesta(false, "Error guardando el examen.", "guardarExamPaciente " + ex.getMessage());
        }

    }
    
    public Respuesta getExamenes(Long idExpediente){
        
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idExpediente", idExpediente);
            Request request = new Request("ExamenesPacienteController/Examenes", "/{idExpediente}", parametros);
            request.get();
            if(request.isError()){
                return new Respuesta(false, request.getError(), "Error al cargar el examen");
            }
            List<ExamenespacienteDto> examenDto = (List<ExamenespacienteDto>) request.readEntity(new GenericType<List<ExamenespacienteDto>>() {
            });
            return new Respuesta(true, "", "", "Examenes", examenDto);
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error cargando el examen ", ex);
            return new Respuesta(false, "Error cargando el examen.", "getExamenes " + ex.getMessage());
        }
        
    }
    public Respuesta eliminarExamen(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("ExamenesPacienteController/eliminarExamen","/{id}",parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(PersonaService.class.getName()).log(Level.SEVERE, "Error eliminando el examen.", ex);
            return new Respuesta(false, "Error eliminando el examen.", "eliminarExamen " + ex.getMessage());
        }
    }
}
