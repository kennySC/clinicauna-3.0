package clinicauna.services;

import clinicauna.model.EncabezadoexpedienteDto;
import clinicauna.model.PersonaDto;
import clinicauna.model.RegistropacientesDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Kenneth Sibaja
 */
public class PacienteService {

    public Respuesta guardarPaciente(RegistropacientesDto paciente) {

        try {
            Respuesta resPers = new PersonaService().guardarPersona(paciente.getIdpers());
            if (resPers.getEstado()) {
                paciente.setIdpers((PersonaDto) resPers.getResultado("Persona"));
                Request request = new Request("PacienteController/guardarPaciente");
                request.post(paciente);
                if (request.isError()) {
                    return new Respuesta(false, request.getError(), "Error al guardar el paciente");
                }
                RegistropacientesDto pacienteDTO = (RegistropacientesDto) request.readEntity(RegistropacientesDto.class);
                if (paciente.getNuevo()) {
                    EncabezadoexpedienteDto expediente = new EncabezadoexpedienteDto();
                    expediente.setIdpaciente(pacienteDTO);
                    Respuesta resExp = new EncabezadoService().guardarExpediente(expediente);
                }
                return new Respuesta(true, "Paciente guardado con exito", "", "Paciente", pacienteDTO);
            } else {
                return new Respuesta(false, "Error al guardar Persona", "");
            }
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error guardando el paciente ", ex);
            return new Respuesta(false, "Error guardando el Paciente.", "guardarPaciente " + ex.getMessage());
        }

    }

    public Respuesta getPacientes(String cedula, String nombre, String apellidos) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("cedula", cedula);
            parametros.put("nombre", nombre);
            parametros.put("apellidos", apellidos);
            Request request = new Request("PacienteController/pacientes", "/{cedula}/{nombre}/{apellidos}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            List<RegistropacientesDto> pacientes = (List<RegistropacientesDto>) request.readEntity(new GenericType<List<RegistropacientesDto>>() {
            });

            return new Respuesta(true, "", "", "Pacientes", pacientes);
        } catch (Exception ex) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error obteniendo pacientes.", ex);
            return new Respuesta(false, "Error obteniendo pacientes.", "getPacientes " + ex.getMessage());
        }

    }

    public Respuesta eliminarPaciente(RegistropacientesDto paciente) {
        try {
            Long id = paciente.getIdpaciente();
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("PacienteController/eliminarPaciente", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            Respuesta resPersona = new PersonaService().eliminarPersona(paciente.idpers.getIdpers());
            if (resPersona.getEstado()) {
                return new Respuesta(true, "", "");
            } else {
                return new Respuesta(false, "", "");
            }
        } catch (Exception ex) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error eliminando el paciente.", ex);
            return new Respuesta(false, "Error eliminando el paciente.", "eliminarPaciente " + ex.getMessage());
        }
    }

}
