
package clinicauna.services;

import clinicauna.model.DetalleexpedienteDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Matthew Miranda
 */
public class DetalleExpedienteService {
    
    public Respuesta guardarDetExpediente(DetalleexpedienteDto detexpdto){
        try{
            Request req = new Request("DetalleExpedienteController/guardarDetalle");
            req.post(detexpdto);
            if (req.isError()) {
                return new Respuesta(false, req.getError(), "");
            }
            DetalleexpedienteDto detexp = (DetalleexpedienteDto) req.readEntity(DetalleexpedienteDto.class);
            return new Respuesta(true, " ", "", "DetExp", detexp);
        }
        catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error guardando el detalle de expediente.", ex);
            return new Respuesta(false, "Error guardando el detalle de expediente.", "GuardarDetExp " + ex.getMessage());
        }
    }
    
    public Respuesta cargarDetExpedientes(Long idExpediente){
        
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idExpediente", idExpediente);
            Request request = new Request("DetalleExpedienteController/Detalles", "/{idExpediente}", parametros);
            request.get();
            if(request.isError()){
                return new Respuesta(false, request.getError(), "Error al cargar los antecedentes");
            }
            List<DetalleexpedienteDto> detexpedientes = (List<DetalleexpedienteDto>) request.readEntity(new GenericType<List<DetalleexpedienteDto>>() {});
            return new Respuesta(true, "", "", "DetExp", detexpedientes);
        } catch (Exception ex) {
            Logger.getLogger(UsuariosService.class.getName()).log(Level.SEVERE, "Error cargando el antecedentes ", ex);
            return new Respuesta(false, "Error cargando los antecedentes.", "cargarAntecedHF " + ex.getMessage());
        }
        
    }
    
}
